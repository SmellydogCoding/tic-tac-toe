import Vue from 'vue'
import Vuetify, { VApp, VContent, VContainer, VList, VListItem, VCard, VDialog, VChip, VFooter } from 'vuetify/lib'
import { Resize } from 'vuetify/lib/directives'

Vue.use(Vuetify)

export default new Vuetify({
  icons: {
    iconfont: 'fa'
  },
  components: {
    VApp,
    VContent,
    VContainer,
    VList,
    VListItem,
    VCard,
    VDialog,
    VChip,
    VFooter
  },
  directives: { Resize }
})
