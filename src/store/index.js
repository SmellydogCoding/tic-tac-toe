import Vue from 'vue'

const state = Vue.observable({
  currentPlayer: '',
  score: { X: 0, O: 0, draw: 0 },
  players: 0,
  difficulity: 0,
  showNewGameDialog: false
})

export const getters = {
  currentPlayer: () => state.currentPlayer,
  score: () => state.score,
  showNewGameDialog: () => state.showNewGameDialog
}

export const actions = {
  setInitialPlayer: (marker) => { state.currentPlayer = marker },
  changePlayer: () => { state.currentPlayer === 'X' ? state.currentPlayer = 'O' : state.currentPlayer = 'X' },
  updateScore: () => { state.score[state.currentPlayer] += 1 },
  resetScore: () => { state.score = { X: 0, Y: 0, draw: 0 } },
  showNewGameDialog: (action) => {
    action === 'hide' ? state.showNewGameDialog = false : state.showNewGameDialog = true
    console.log('called')
  },
  newGame: (players, p1Marker, difficulity) => {
    state.players = players
    state.currentPlayer = p1Marker
    state.difficulity = difficulity
  }
}

export default { getters, actions }
